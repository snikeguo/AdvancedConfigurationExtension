﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedConfigurationExtensionDemoForWinform
{
    public enum ElementType
    {
        String,
        Enum,
    }
    public class Element
    {
        public object Value { get; set; }
        public ElementType FiledType { get; set; }

        /// <summary>
        /// 当FiledType是Enum，这个集合表示Value的枚举值
        /// </summary>
        public List<string> EnumValueDisplayString { get; set; }

        public string DisplayString { get; set; }

    }
    public interface IPropertyConfig:ICsProcess
    {
        string CurrentPlatform { get; set; }
        List<Element> Elements { get; set; }
    }
}

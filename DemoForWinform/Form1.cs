﻿using CSScriptLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvancedConfigurationExtensionDemoForWinform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Controls.Clear();

            IPropertyConfig config = null;
            using (SimpleAsmProbing.For(Assembly.GetExecutingAssembly().Location.GetDirName()))
            {
                await Task.Run(() =>
                {
                    config = CSScript.Evaluator.LoadFile<IPropertyConfig>("MyConfigDefine.cs");
                });
            }
            config.CurrentPlatform = comboBox1.SelectedItem.ToString();
            config.Process();

            for (int i = 0; i < config.Elements.Count(); i++)
            {
                var element = config.Elements[i];
                Label displayLable = new Label();
                displayLable.Text = element.DisplayString;
                flowLayoutPanel1.Controls.Add(displayLable);
                if(element.FiledType== ElementType.Enum)
                {
                    ComboBox comboBox = new ComboBox();
                    comboBox.Items.AddRange(element.EnumValueDisplayString.ToArray());
                    flowLayoutPanel1.Controls.Add(comboBox);
                }
                else
                {
                    TextBox tb = new TextBox();
                    flowLayoutPanel1.Controls.Add(tb);
                }
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using AdvancedConfigurationExtensionDemoForWinform;
//namespace AdvancedConfigurationExtensionDemoForWinform
//{
    public class MyConfigDefine : IPropertyConfig
    {
        public List<Element> Elements { get; set; }
        public string CurrentPlatform { get; set; }

        public void Process()
        {
            Elements = new List<Element>();
            if(CurrentPlatform== "Android")
            {
                Element e = new Element();
                e.FiledType = ElementType.Enum;
                e.EnumValueDisplayString = new List<string>();
                e.EnumValueDisplayString.Add("Android7.0 AndroidSdk24");
                e.EnumValueDisplayString.Add("Android7.1 AndroidSdk25");
                e.EnumValueDisplayString.Add("Android8.0 AndroidSdk26");
                e.DisplayString= "Android SDK 版本号";
                e.Value = (ElementType)0;
                Elements.Add(e);
            }
            Element cppPath = new Element();
            cppPath.FiledType = ElementType.String;
            cppPath.DisplayString = "C++路径";
            Elements.Add(cppPath);
        }
    }
//}

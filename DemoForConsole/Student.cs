﻿using System;
namespace RoslynTest1
{
    public interface ICsProcess
    {
        void Process();
    }
    public interface IStudent:ICsProcess
    {
        string Name { get; set; }
        int Age { get; set;}
        
    }
    
}

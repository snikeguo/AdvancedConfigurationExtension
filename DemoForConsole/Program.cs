﻿using CSScriptLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RoslynTest1
{
    class Program
    {
        static void Main(string[] args)
        {
            //CSScriptEvaluatorApi.HostApp.Test();
            IStudent config = null;
            using (SimpleAsmProbing.For(Assembly.GetExecutingAssembly().Location.GetDirName()))
            {
                config = CSScript.Evaluator.LoadFile<IStudent>("config.cs");                
            }
            config.Process();
            Console.WriteLine($"Name:{config.Name},Age:{config.Age}");
            Console.ReadLine();
        }
    }
}
